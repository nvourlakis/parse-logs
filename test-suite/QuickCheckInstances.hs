module QuickCheckInstances () where

import Test.Tasty.QuickCheck

import Data.Fixed (Pico)
import Data.Time (fromGregorian, TimeOfDay(..), LocalTime(..))

import LogParser (LogEntry(..), IP(..), Source(..), Product(..))

ipPart :: Gen String
ipPart = show <$> choose ((0, 255) :: (Int, Int))

year :: Gen Integer
year = choose ((1970, 2200) :: (Integer, Integer))

month :: Gen Int
month = choose ((1, 12) :: (Int, Int))

day :: Gen Int
day = choose ((1, 31) :: (Int, Int))

hour :: Gen Int
hour = choose ((0, 23) :: (Int, Int))

minutes :: Gen Int
minutes = choose ((0, 59) :: (Int, Int))

seconds :: Gen Pico
seconds = fromIntegral <$> choose ((0, 59) :: (Int, Int))

instance Arbitrary LogEntry where
    arbitrary = LogEntry <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary LocalTime where
    arbitrary =
        LocalTime <$>
            (fromGregorian <$> year <*> month <*> day)
        <*> (TimeOfDay <$> hour <*> minutes <*> seconds)

instance Arbitrary IP where
    arbitrary = IP <$> ipPart <*> ipPart <*> ipPart <*> ipPart

instance Arbitrary Product where
    arbitrary = oneof
        [ pure Mouse
        , pure Keyboard
        , pure Monitor
        , pure Speakers
        ]

instance Arbitrary Source where
    arbitrary = oneof
        [ pure Internet
        , pure Friend
        , pure NoAnswer
        ]
