import Test.Tasty
import Test.Tasty.QuickCheck as QC

import Text.ParserCombinators.Parsec (parse)
import Text.PrettyPrint (render)

import QuickCheckInstances()

import LogParser
import LogPrettyPrinter

main :: IO ()
main = defaultMain props

props :: TestTree
props = testGroup "LogParser"
    [ testProperty "parse a single standard log entry" checkLogEntryParser
    , testProperty "parse a list of standard log entries" checkLogParser
    ]

checkLogEntryParser :: LogEntry -> Bool
checkLogEntryParser le =
    case parse logEntryParser "standard log entry" (render (logEntry le)) of
        Left _ -> False
        Right result -> result == le

checkLogParser :: Log -> Bool
checkLogParser le =
    case parse logParser "standard log entries" (render (prettyLog le)) of
        Left _ -> False
        Right result -> result == le
